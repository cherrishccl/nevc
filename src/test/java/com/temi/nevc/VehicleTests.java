package com.temi.nevc;

import com.temi.nevc.common.util.DateUtil;
import com.temi.nevc.common.util.ExcelUtil;
import com.temi.nevc.entity.TVehicle;
import com.temi.nevc.repo.TVehicleRepo;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import javax.annotation.Resource;
import java.util.ArrayList;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class VehicleTests {
    @Test
    public void test(){
//importTVehicle();
//        changeVehicle(8601002);
//        parseDate();
//        readAndImport();
    }

    private void readAndImport(){
        String basePath = "C:\\Users\\Luo_xuri\\Downloads\\nevc-lyb\\";
        long begin = System.currentTimeMillis();
        String startTime = "2018-08-28 05:00:00";

        for (int i = 1; i <= 50; i++) {
            String filePath = null;
            if(i>10){
                filePath =  basePath + i +".xls";
            }else {
                filePath =  basePath + i +".xlsx";
            }
            long begin1 = System.currentTimeMillis();
            try {
                List<TVehicle> list = ExcelUtil.importExcel(filePath, 0, 1, TVehicle.class);
                List<TVehicle> newList = new ArrayList<>(list.size());
                if(list.size() > 6000){
                    startTime = "2018-08-28 02:30:00";
                }
                if(list.size() < 5000){
                    startTime = "2018-08-28 07:00:00";
                }
                if(null != list && list.size() > 0){
                    Long startTimeMills = DateUtil.getTimeStamp(startTime);
                    for(TVehicle vehicle : list){
                        String timeStr = DateUtil.long2TimeString(startTimeMills);
//                        vehicle.setRawTime(vehicle.getCurTime());
                        vehicle.setCurTime(timeStr);
                        vehicle.setCreateTime(startTimeMills);
                        startTimeMills = startTimeMills + 10000L;
                        newList.add(vehicle);
                    }
                    System.out.println("save begin" + list.get(0).getCarId());
                    tVehicleRepo.saveAll(newList);
                    System.out.println("save end" + list.get(0).getCarId());
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            long end1 = System.currentTimeMillis();
            System.out.println("************* total time: "+ (end1 - begin1));

        }
        long end = System.currentTimeMillis();
        System.out.println("************* total time: "+ (end - begin));
    }
    int[] carIds = new int[]{8601001, 8601002, 8601003, 8601004, 8601005, 8601006, 8601007, 8601008, 8601009, 8601010, 8601051, 8601052, 8601053, 8601054, 8601055,8601056, 8601057, 8601058, 8601059, 8601060};

    public void parseDate(){
        for(int carId : carIds){
            changeVehicle(carId);
        }
    }

    private void changeVehicle(Integer carId){
        List<TVehicle> list=  tVehicleRepo.findByCarId(carId);
        String startTime = "2018-08-28 05:00:00";
        List<TVehicle> newList = new ArrayList<>(list.size());
        if(list.size() > 6000){
            startTime = "2018-08-28 03:00:00";
        }
        if(list.size() < 5000){
            startTime = "2018-08-28 07:00:00";
        }
        if(null != list && list.size() > 0){
            Long startTimeMills = DateUtil.getTimeStamp(startTime);
            for (TVehicle  tVehicle : list){
                String timeStr = DateUtil.long2TimeString(startTimeMills);
                tVehicle.setCurTime(timeStr);
                tVehicle.setCreateTime(startTimeMills);
                startTimeMills = startTimeMills + 10000L;
                newList.add(tVehicle);
            }
            System.out.println("carId"+ carId + "startTime ----------- " + startTime);
            tVehicleRepo.saveAll(newList);
        }
    }

    @Resource
    private TVehicleRepo tVehicleRepo;
    private void importTVehicle() {
        String filePath = "C:\\Users\\oxchains\\Desktop\\NEV\\2 - 副本.xls";
        filePath = "D:\\docs\\cars\\1-2\\10.xlsx";
        filePath = "D:\\docs\\cars\\1-2 8 9\\11-5001.xls";
        try {
            List<TVehicle> list = ExcelUtil.importExcel(filePath, 0, 1, TVehicle.class);
            System.out.println(list.size());
            tVehicleRepo.saveAll(list);
            /*int i = 0;
            if(null != list && list.size() > 0){
                for(TVehicle vehicle : list){
                    tVehicleRepo.save(vehicle);
                    i++;
                }
            }*/
//            System.out.println(i);
            System.out.println(list.size());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void importVehicle() {
        String filePath = "/var/temi/vehicle.xls";
        filePath = "D:\\docs\\cars\\1-2\\1.xlsx";
        try {
            List<Vehicle> list = ExcelUtil.importExcel(filePath, 0, 1, Vehicle.class);
            System.out.println(list.size());

            if(null != list && list.size() > 0){
                for(Vehicle vehicle : list){
                    com.temi.nevc.entity.Vehicle tvo = new com.temi.nevc.entity.Vehicle();

                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
