package com.temi.nevc;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

/**
 * @author ccl
 * @time 2018-08-20 14:22
 * @name Vehicle
 * @desc:
 */
@Data
public class Vehicle {
    @Excel(name = "时间")
    private String curTime;
    @Excel(name = "电压")
    private Double voltage;
    @Excel(name = "能量")
    private Double energy;
    @Excel(name = "速度")
    private Double speed;
    @Excel(name = "里程数A")
    private Double odoa;
    @Excel(name = "电池能量")
    private Double battery;
    @Excel(name = "里程数")
    private Double odo;


}
