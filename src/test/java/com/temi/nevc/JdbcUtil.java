package com.temi.nevc;

import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * @author cherrish
 * @time 2018-09-26 16:25
 * @name JdbcUtil
 * @desc:
 */
public class JdbcUtil {
    private static final Logger LOGGER = Logger.getLogger(JdbcUtil.class.getName());
    protected static String driver;
    protected static String url;
    protected static String username;
    protected static String password;

    static {
        try {
            ClassLoader classLoader = JdbcUtil.class.getClassLoader();
            InputStream inputStream = classLoader.getResourceAsStream("db.properties");
            Properties properties = new Properties();
            properties.load(inputStream);
            driver = properties.getProperty("datasource.driver");
            url = properties.getProperty("datasource.url");
            username = properties.getProperty("datasource.username");
            password = properties.getProperty("datasource.password");
        } catch (IOException e) {
            e.printStackTrace();
            System.out.println("read db.properties error or file not exist");
        }
    }

    public static Connection getConnection(){
        Connection connection = null;
        try {
            Class.forName(driver);
            connection = DriverManager.getConnection(url, username, password);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("driver class not found");
        } catch (SQLException e) {
            e.printStackTrace();
            System.out.println("sql connection error");
        }
        return connection;
    }
    public static void closeResultSet(ResultSet resultSet){
        if(null != resultSet){
            try {
                resultSet.close();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("resultSet close error");
            }
        }
    }

    public static void closePreparedStatement( PreparedStatement preparedStatement){
        if(null != preparedStatement){
            try {
                preparedStatement.close();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("preparedStatement close error");
            }
        }
    }
    public static void closeConnection(Connection connection){

        if(null != connection){
            try {
                connection.close();
            } catch (SQLException e) {
                e.printStackTrace();
                System.out.println("connection close error");
            }
        }
    }
}
