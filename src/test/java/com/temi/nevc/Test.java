package com.temi.nevc;

import com.temi.nevc.common.util.DateUtil;
import com.temi.nevc.common.util.ExcelUtil;

import java.util.Date;
import java.util.List;
import java.util.Random;

/**
 * @author ccl
 * @time 2018-09-26 16:04
 * @name Test
 * @desc:
 */
public class Test {
    public static void main(String[] args) {

        Long l = 1535403600000L + 31 * 24 * 60 * 60 * 1000L;
        System.out.println(31 * 24 * 60 * 60 * 1000L);
        l = 1538082000000L;
        Date date = new Date(1538254800000L);
        System.out.println("date = " + date);

        String curTime = DateUtil.getPresentDate();
        System.out.println(curTime);
        Long time = DateUtil.getTimeStamp(curTime);
        time = time / 1000L;
        System.out.println(time);

        long t = System.currentTimeMillis();
       int pageNum = (t/1000) % 10 == 0 ? 10 : (int)(t % 10);
        System.out.println( "-----" +pageNum);

       /* String filePath = "/var/temi/vehicle.xls";
        filePath = "D:\\docs\\cars\\1-2\\1.xlsx";
        try {
           List<Vehicle> list = ExcelUtil.importExcel(filePath, 0, 1, Vehicle.class);
            System.out.println(list.size());
            System.out.println(list);
        } catch (Exception e) {
            e.printStackTrace();
        }*/

        System.out.println(Math.random());

        for(int i = 0; i < 20; i++){
            Random random = new Random();
            System.out.print(random.nextInt(10) + "   ");
        }
    }
}
