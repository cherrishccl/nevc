package com.temi.nevc;

import javax.sql.DataSource;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.sql.SQLFeatureNotSupportedException;
import java.util.LinkedList;
import java.util.logging.Logger;

/**
 * @author cherrish
 * @time 2018-09-26 16:42
 * @name JdbcDataSource
 * @desc:
 */
public class JdbcDataSource implements DataSource {

    private static LinkedList<Connection> pool = new LinkedList<>();
    // 初始化
    static{
        for (int i = 0; i < 5; i++) {
            Connection conn = JdbcUtil.getConnection();
            pool.add(conn);
        }
    }

    @Override
    public Connection getConnection() throws SQLException {
        Connection connection = null;
        if(pool.size()==0){
            for (int i = 0; i < 5; i++) {
                connection = JdbcUtil.getConnection();
                pool.add(connection);
            }
        }
        connection = pool.remove(0);
        return connection;
    }

    public void laybackConnection(Connection connection){
        pool.add(connection);
    }
    @Override
    public Connection getConnection(String username, String password) throws SQLException {
        return null;
    }

    @Override
    public <T> T unwrap(Class<T> iface) throws SQLException {
        return null;
    }

    @Override
    public boolean isWrapperFor(Class<?> iface) throws SQLException {
        return false;
    }

    @Override
    public PrintWriter getLogWriter() throws SQLException {
        return null;
    }

    @Override
    public void setLogWriter(PrintWriter out) throws SQLException {

    }

    @Override
    public void setLoginTimeout(int seconds) throws SQLException {

    }

    @Override
    public int getLoginTimeout() throws SQLException {
        return 0;
    }

    @Override
    public Logger getParentLogger() throws SQLFeatureNotSupportedException {
        return null;
    }
}
