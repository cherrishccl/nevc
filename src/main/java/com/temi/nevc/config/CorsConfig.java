package com.temi.nevc.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * @author ccl
 * @time 2018-09-26 10:45
 * @name CorsConfig
 * @desc:
 */
@Configuration
public class CorsConfig implements WebMvcConfigurer {
    @Override
    public void addCorsMappings(CorsRegistry registry) {
//设置允许跨域的路径
        registry.addMapping("/**")
                //设置允许跨域请求的域名
                .allowedOrigins("*")
                //设置允许跨域请求方式,或为allowedMethods("*")
                .allowedMethods("GET", "POST", "PUT", "OPTIONS", "DELETE")
                //是否允许证书 2.0不再默认开启
                .allowCredentials(true)
                //允许所有header
                .allowedHeaders("*")
                //跨域允许时间
                .maxAge(3600);
    }
}

