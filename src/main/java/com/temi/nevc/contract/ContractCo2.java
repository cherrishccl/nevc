package com.temi.nevc.contract;

import lombok.extern.slf4j.Slf4j;
import org.web3j.abi.FunctionEncoder;
import org.web3j.abi.FunctionReturnDecoder;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.abi.datatypes.generated.Uint32;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.RawTransaction;
import org.web3j.crypto.TransactionEncoder;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.DefaultBlockParameterName;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.request.Transaction;
import org.web3j.protocol.core.methods.response.EthCall;
import org.web3j.protocol.core.methods.response.EthGetTransactionCount;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.http.HttpService;
import org.web3j.utils.Convert;
import org.web3j.utils.Numeric;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author ccl
 * @time 2018-09-28 16:50
 * @name ContractCo2
 * @desc:
 */
@Slf4j
public class ContractCo2 {
    protected static final BigInteger GAS_PRICE = Convert.toWei("22", Convert.Unit.GWEI).toBigInteger();
    protected static final BigInteger GAS_LIMIT = new BigInteger("4500000");
    protected static final String SERVER_URL = "http://rpc.themiscan.io";
    protected final Web3j web3j = Web3j.build(new HttpService(SERVER_URL));
    private static final String CONTRACT_ADDRESS = "0x7ef9c094936da90eeb8a842f82e3379173e0d921";

    private static final String FUNC_GETELECTRICCARCO2 = "getElectricCarCO2";

    private static final String FUNC_GETCOMBUSTIONCARCO2 = "getCombustionCarCO2";

    private static final String FUNC_UPDATECARINFO = "updateCarInfo";

    private Credentials credentials;

    private ContractCo2(String priKey) {
        credentials = Credentials.create(priKey);
    }
    private static ContractCo2 contractCo2 = null;
    public static ContractCo2 getInstance(String priKey){
        if(null == contractCo2){
            contractCo2 = new ContractCo2(priKey);
        }
        return contractCo2;
    }

    public String updateCarInfo(Integer carId, Long time, Long mile, Long energy, String license, String model){
        BigInteger cid = new BigInteger(carId.toString());
        BigInteger t = new BigInteger(time.toString());
        BigInteger m = new BigInteger(mile.toString());
        BigInteger e = new BigInteger(energy.toString());
        return updateCarInfo(new Uint256(cid), new Uint256(t), new Uint32(m), new Uint32(e), new Utf8String(license), new Utf8String(model) );
    }


    public Double getElectricCarCO2(Integer carID, Long time) throws IOException {
        final Function function = new Function(FUNC_GETELECTRICCARCO2,
                Arrays.<Type>asList(new Uint256(new BigInteger(carID.toString())), new Uint256(time)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        String result = query(null, CONTRACT_ADDRESS, function);
        List<Type> types = FunctionReturnDecoder.decode(result, function.getOutputParameters());
        if (null != types && types.size() > 0) {
            result = types.get(0).getValue().toString();
        }
        BigInteger bigInteger = new BigInteger(result);
        return bigInteger.doubleValue() / 100000000L;

    }
    public Double getCombustionCarCO2(Integer carID, Long time) throws IOException {
        final Function function = new Function(FUNC_GETCOMBUSTIONCARCO2,
                Arrays.<Type>asList(new Uint256(new BigInteger(carID.toString())), new Uint256(0L), new Uint256(time)),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        String result = query(null, CONTRACT_ADDRESS, function);
        List<Type> types = FunctionReturnDecoder.decode(result, function.getOutputParameters());
        if (null != types && types.size() > 0) {
            result = types.get(0).getValue().toString();
        }
        BigInteger bigInteger = new BigInteger(result);
        return bigInteger.doubleValue() / 10000000000000000L;
    }
    private String updateCarInfo(Uint256 carID, Uint256 time, Uint32 __milage, Uint32 __energy, Utf8String carNo, Utf8String carType){
        final Function function = new Function(
                FUNC_UPDATECARINFO,
                Arrays.<Type>asList(carID, time, __milage, __energy, carNo, carType),
                Collections.<TypeReference<?>>emptyList());
        String hash = transaction(CONTRACT_ADDRESS, function, credentials , new BigInteger("0"));
        return sendRawTransaction(hash);
    }
    private String transaction(String to, Function function, Credentials credentials, BigInteger value) {
        String encodedFunction = FunctionEncoder.encode(function);
        BigInteger nonce = getNonce(credentials.getAddress(), DefaultBlockParameterName.PENDING);
        RawTransaction rawTransaction = RawTransaction.createTransaction(nonce, GAS_PRICE, GAS_LIMIT, to/*HOSTER_CONTRACT_ADDRESS*/, value, encodedFunction);
        byte[] signedMessage = TransactionEncoder.signMessage(rawTransaction, credentials);
        String hexValue = Numeric.toHexString(signedMessage);
        log.info("signed message hex: {}", hexValue);
        return hexValue;
    }

    private String query(String from, String to, Function function) throws IOException {
        String encodedFunction = FunctionEncoder.encode(function);
        Transaction ethCallTransaction = Transaction.createEthCallTransaction(from, to, encodedFunction);
        EthCall send = web3j.ethCall(ethCallTransaction, DefaultBlockParameterName.LATEST).send();
        String result = send.getResult();
        log.info("query result: {}", result.toString());
        return result;
    }

    private String sendRawTransaction(String hex) {
        EthSendTransaction ethSendTransaction = null;
        String transactionHash = null;
        try {
            ethSendTransaction = web3j.ethSendRawTransaction(hex).send();
            if (ethSendTransaction.hasError()) {
                log.error("Transaction Error: {}", ethSendTransaction.getError().getMessage());
            } else {
                transactionHash = ethSendTransaction.getTransactionHash();
                log.info("Transaction Hash: {}", transactionHash);
            }
        } catch (IOException e) {
            log.error("Send Raw Transaction Error: {}", e);
        }
        return transactionHash;
    }
    private BigInteger getNonce(String address, DefaultBlockParameterName parameterName) {
        BigInteger nonce = null;
        try {
            EthGetTransactionCount count = web3j.ethGetTransactionCount(address, parameterName).send();
            nonce = count.getTransactionCount();
            log.info("address: {} ---> nonce: {}", address, nonce);
        } catch (IOException e) {
            log.error("Get Address: {} ---> Nonce Error: {}", address, e);
        }
        return nonce;
    }

    public static void main(String[] args) {
        String hash = ContractCo2.getInstance("320c67a11b523cc3ff174d36692000a2d5da3b15d185ec188f325744bcba4c0a").updateCarInfo(8601001, 1538081000000L, 7222600L, 41115L, "----", "ESC");
        System.out.println(hash);
    }
}
