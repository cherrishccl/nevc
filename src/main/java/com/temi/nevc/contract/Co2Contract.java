package com.temi.nevc.contract;

import java.io.IOException;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.Arrays;
import java.util.Collections;
import java.util.concurrent.CompletableFuture;

import com.temi.nevc.common.util.DateUtil;
import org.web3j.abi.TypeReference;
import org.web3j.abi.datatypes.Function;
import org.web3j.abi.datatypes.Type;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.abi.datatypes.generated.Uint32;
import org.web3j.abi.datatypes.generated.Uint96;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.RemoteCall;
import org.web3j.protocol.core.methods.response.EthSendRawTransaction;
import org.web3j.protocol.core.methods.response.EthSendTransaction;
import org.web3j.protocol.core.methods.response.EthTransaction;
import org.web3j.protocol.core.methods.response.TransactionReceipt;
import org.web3j.protocol.http.HttpService;
import org.web3j.tx.Contract;
import org.web3j.tx.TransactionManager;
import org.web3j.utils.Convert;

/**
 * <p>Auto generated code.
 * <p><strong>Do not modify!</strong>
 * <p>Please use the <a href="https://docs.web3j.io/command_line.html">web3j command line tools</a>,
 * or the org.web3j.codegen.SolidityFunctionWrapperGenerator in the 
 * <a href="https://github.com/web3j/web3j/tree/master/codegen">codegen module</a> to update.
 *
 * <p>Generated with web3j version 3.5.0.
 */
public class Co2Contract extends Contract {
    private static final String BINARY = "608060405260a06040519081016040528061245f67ffffffffffffffff168152602001620b97f867ffffffffffffffff168152602001610140604051908101604052806116d067ffffffffffffffff1667ffffffffffffffff168152602001600067ffffffffffffffff168152602001600067ffffffffffffffff168152602001600067ffffffffffffffff168152602001600067ffffffffffffffff168152602001600067ffffffffffffffff168152602001600067ffffffffffffffff168152602001600067ffffffffffffffff168152602001600067ffffffffffffffff168152602001600067ffffffffffffffff1681525081526020016102a367ffffffffffffffff1681526020016110d367ffffffffffffffff168152506000808201518160000160006101000a81548167ffffffffffffffff021916908367ffffffffffffffff16021790555060208201518160000160086101000a81548167ffffffffffffffff021916908367ffffffffffffffff16021790555060408201518160010190600a610192929190610246565b5060608201518160040160006101000a81548167ffffffffffffffff021916908367ffffffffffffffff16021790555060808201518160040160086101000a81548167ffffffffffffffff021916908367ffffffffffffffff16021790555050503480156101ff57600080fd5b5033600660006101000a81548173ffffffffffffffffffffffffffffffffffffffff021916908373ffffffffffffffffffffffffffffffffffffffff16021790555061032c565b82600a6003016004900481019282156102e45791602002820160005b838211156102ae57835183826101000a81548167ffffffffffffffff021916908367ffffffffffffffff1602179055509260200192600801602081600701049283019260010302610262565b80156102e25782816101000a81549067ffffffffffffffff02191690556008016020816007010492830192600103026102ae565b505b5090506102f191906102f5565b5090565b61032991905b8082111561032557600081816101000a81549067ffffffffffffffff0219169055506001016102fb565b5090565b90565b6104648061033b6000396000f300608060405260043610610057576000357c0100000000000000000000000000000000000000000000000000000000900463ffffffff16806348754c311461005c57806379228fa7146100a7578063d4d6df5d146100fc575b600080fd5b34801561006857600080fd5b5061009160048036038101908080359060200190929190803590602001909291905050506101df565b6040518082815260200191505060405180910390f35b3480156100b357600080fd5b506100e6600480360381019080803590602001909291908035906020019092919080359060200190929190505050610287565b6040518082815260200191505060405180910390f35b34801561010857600080fd5b506101dd6004803603810190808035906020019092919080359060200190929190803563ffffffff169060200190929190803563ffffffff169060200190929190803590602001908201803590602001908080601f0160208091040260200160405190810160405280939291908181526020018383808284378201915050505050509192919290803590602001908201803590602001908080601f0160208091040260200160405190810160405280939291908181526020018383808284378201915050505050509192919290505050610348565b005b6000806000800160009054906101000a900467ffffffffffffffff1667ffffffffffffffff166000800160089054906101000a900467ffffffffffffffff1660056000878152602001908152602001600020600086815260200190815260200160002060000160049054906101000a900463ffffffff1663ffffffff160267ffffffffffffffff1681151561027057fe5b0467ffffffffffffffff1690508091505092915050565b600080600060040160089054906101000a900467ffffffffffffffff16600060040160009054906101000a900467ffffffffffffffff16600060010186600a811015156102d057fe5b600491828204019190066008029054906101000a900467ffffffffffffffff1660056000898152602001908152602001600020600087815260200190815260200160002060000160009054906101000a900463ffffffff1663ffffffff1602020267ffffffffffffffff169050809150509392505050565b600660009054906101000a900473ffffffffffffffffffffffffffffffffffffffff1673ffffffffffffffffffffffffffffffffffffffff163373ffffffffffffffffffffffffffffffffffffffff161415156103a457600080fd5b8360056000888152602001908152602001600020600087815260200190815260200160002060000160006101000a81548163ffffffff021916908363ffffffff1602179055508260056000888152602001908152602001600020600087815260200190815260200160002060000160046101000a81548163ffffffff021916908363ffffffff1602179055505050505050505600a165627a7a723058208dbb239c9bfade9bd5a0fc92c23d93162ede6b5e2a6b61ae72076fdfd24ee9300029";

    public static final String FUNC_GETELECTRICCARCO2 = "getElectricCarCO2";

    public static final String FUNC_GETCOMBUSTIONCARCO2 = "getCombustionCarCO2";

    public static final String FUNC_UPDATECARINFO = "updateCarInfo";

    protected Co2Contract(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    protected Co2Contract(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        super(BINARY, contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public RemoteCall<Uint256> getElectricCarCO2(Uint256 carID, Uint256 time) {
        final Function function = new Function(FUNC_GETELECTRICCARCO2,
                Arrays.<Type>asList(carID, time),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function);
    }

    public RemoteCall<Uint256> getCombustionCarCO2(Uint256 carID, Uint256 typeid, Uint256 time) {
        final Function function = new Function(FUNC_GETCOMBUSTIONCARCO2,
                Arrays.<Type>asList(carID, typeid, time),
                Arrays.<TypeReference<?>>asList(new TypeReference<Uint256>() {}));
        return executeRemoteCallSingleValueReturn(function);
    }

    public RemoteCall<TransactionReceipt> updateCarInfo(Uint256 carID, Uint256 time, Uint32 __milage, Uint32 __energy, Utf8String carNo, Utf8String carType) {
        final Function function = new Function(
                FUNC_UPDATECARINFO,
                Arrays.<Type>asList(carID, time, __milage, __energy, carNo, carType),
                Collections.<TypeReference<?>>emptyList());
        return executeRemoteCallTransaction(function);
    }

    public static RemoteCall<Co2Contract> deploy(Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(Co2Contract.class, web3j, credentials, gasPrice, gasLimit, BINARY, "");
    }

    public static RemoteCall<Co2Contract> deploy(Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return deployRemoteCall(Co2Contract.class, web3j, transactionManager, gasPrice, gasLimit, BINARY, "");
    }

    public static Co2Contract load(String contractAddress, Web3j web3j, Credentials credentials, BigInteger gasPrice, BigInteger gasLimit) {
        return new Co2Contract(contractAddress, web3j, credentials, gasPrice, gasLimit);
    }

    public static Co2Contract load(String contractAddress, Web3j web3j, TransactionManager transactionManager, BigInteger gasPrice, BigInteger gasLimit) {
        return new Co2Contract(contractAddress, web3j, transactionManager, gasPrice, gasLimit);
    }

    public static void main(String[] args) throws Exception {
        Web3j web3j = Web3j.build(new HttpService(/*"http://rpc.themiscan.io"*/"http://localhost:8555"));
        Credentials credentials = WalletUtils.loadCredentials("Temi0409nevc", "D:\\deps\\gthemis\\NEVC.json");
        Co2Contract co2Contract = deploy(web3j, credentials, new BigInteger("15000000"), new BigInteger("4700000")).send();
//        Co2Contract co2Contract = deploy(web3j, credentials, Convert.toWei("25", Convert.Unit.GWEI).toBigInteger(), new BigInteger("4500000")).send();
//        System.out.println(co2Contract);//0x7ef9c094936da90eeb8a842f82e3379173e0d921
//        Co2Contract co2Contract = load("0x7ef9c094936da90eeb8a842f82e3379173e0d921", web3j, credentials, new BigInteger("15000000"), new BigInteger("4700000"));
//        Co2Contract co2Contract = load("0x7ef9c094936da90eeb8a842f82e3379173e0d921", web3j, credentials, Convert.toWei("22", Convert.Unit.GWEI).toBigInteger(), new BigInteger("450000"));

        BigInteger carId = new BigInteger("8601001");
        BigInteger time = new BigInteger("1538199760000");
        BigInteger mile = new BigInteger("7222600");//里程数 * 100
        BigInteger energy = new BigInteger("41115");// 电池能量 * 1000

        while (true){
            for(int i =0; i < 60; i++){
                ContractCo2.getInstance("320c67a11b523cc3ff174d36692000a2d5da3b15d185ec188f325744bcba4c0a").updateCarInfo(carId.intValue(), time.longValue(), mile.longValue(), energy.longValue(), "京A·D0***5", "EU5");
                System.out.println("************************" + i);
            }
            Thread.sleep(5000);
        }
/*        Uint256 cco2 = co2Contract.getCombustionCarCO2(new Uint256(carId), new Uint256(new BigInteger("0")), new Uint256(time)).send();
        Uint256 co2 = co2Contract.getElectricCarCO2(new Uint256(carId), new Uint256(time)).send();*/
//        long tt = 1538200980000L;
//        int cid = 8601001;
     /* for(int i = 0; i < 5; i++){

          Double preco2 = ContractCo2.getInstance("320c67a11b523cc3ff174d36692000a2d5da3b15d185ec188f325744bcba4c0a").getElectricCarCO2(cid, tt - 10000L);
          Double precomco2 = ContractCo2.getInstance("320c67a11b523cc3ff174d36692000a2d5da3b15d185ec188f325744bcba4c0a").getCombustionCarCO2(cid , tt - 10000L);

          Double co2 = ContractCo2.getInstance("320c67a11b523cc3ff174d36692000a2d5da3b15d185ec188f325744bcba4c0a").getElectricCarCO2(cid, tt);
          Double comco2 = ContractCo2.getInstance("320c67a11b523cc3ff174d36692000a2d5da3b15d185ec188f325744bcba4c0a").getCombustionCarCO2(cid , tt);
          System.out.print("*************************time="+ DateUtil.long2TimeString(tt) +", preco2="+ preco2+ ", co2="+ co2 + ", precomco2="+ precomco2 + ", comco2="+ comco2);
          System.out.println("---diffco2=" + (preco2 - co2)+ ", diffcomco2="+ (comco2-precomco2));
          tt += 10000L;
      }*/
//        System.out.println(receipt);

/*        String s = Web3jHandler.signedEthTransactionData("320c67a11b523cc3ff174d36692000a2d5da3b15d185ec188f325744bcba4c0a",
                "0xac9f877bd62a9806dcc863f85e631ad0853056c1", new BigInteger("5"), new BigDecimal("20"));
        EthSendTransaction transaction = web3j.ethSendRawTransaction(s).send();
        System.out.println(transaction);*/
    }
}
