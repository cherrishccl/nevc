package com.temi.nevc.repo;

import com.temi.nevc.entity.VehicleScore;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author ccl
 * @time 2018-09-28 17:48
 * @name VehicleScoreRepo
 * @desc:
 */
public interface VehicleScoreRepo extends JpaRepository<VehicleScore, Long> {
    List<VehicleScore> findByCreateTime(Long time);
    List<VehicleScore> findByScoreIsNullAndCreateTimeGreaterThan(Long time);
}
