package com.temi.nevc.repo;

import com.temi.nevc.entity.TVehicle;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author ccl
 * @time 2018-09-27 9:39
 * @name VehicleRepo
 * @desc:
 */
@Repository
public interface TVehicleRepo extends JpaRepository<TVehicle, Long> {
    List<TVehicle> findByCarId(Integer carId);
}
