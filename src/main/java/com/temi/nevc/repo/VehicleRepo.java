package com.temi.nevc.repo;

import com.temi.nevc.entity.TVehicle;
import com.temi.nevc.entity.Vehicle;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

/**
 * @author ccl
 * @time 2018-09-27 9:39
 * @name VehicleRepo
 * @desc:
 */
public interface VehicleRepo extends JpaRepository<Vehicle, Long> {
    List<TVehicle> findByCarId(Integer carId);
    List<Vehicle> findByCreateTime(Long createTime);
    Page<Vehicle> findByCreateTime(Long createTime, Pageable pageable);
}
