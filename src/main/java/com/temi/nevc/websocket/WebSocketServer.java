package com.temi.nevc.websocket;

import com.alibaba.fastjson.JSON;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.websocket.*;
import javax.websocket.server.PathParam;
import javax.websocket.server.ServerEndpoint;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicInteger;

/**
 * @author ccl
 * @time 2018-09-21 11:37
 * @name WebSocketServer
 * @desc:
 */
@Slf4j
@Component
//@ServerEndpoint(value = "/ws/{cid}")
public class WebSocketServer {
    private static final AtomicInteger onlineCount = new AtomicInteger(0);
    private static ConcurrentHashMap<String, Session> webSocketServersMap = new ConcurrentHashMap<>();
    private Session session;
    private String cid = null;

    @OnOpen
    public void onOpen(Session session, @PathParam("cid") String cid){
        this.session = session;
        this.cid = cid;
        webSocketServersMap.put(cid, session);
        int count = onlineCount.incrementAndGet();
        log.info("{}-{} connected, total connections: {}", cid, session.getId(), count);
        this.cid = cid;
        Message message = new Message("000000", cid, "connected success");
        sendMessage(cid, session, JSON.toJSONString(message));
    }

    @OnClose
    public void onClose(Session session, @PathParam("cid") String cid){
        webSocketServersMap.remove(cid);
        int count = onlineCount.decrementAndGet();
        log.info("{}-{} disconnected, total connections: {}", cid, session.getId(), count);
    }

    @OnMessage
    public void onMessage(Session session, String message,@PathParam("cid") String cid){
        log.info("{}-{} send message: {}", cid, session.getId(), message);
        try{
            Message websocketMessage = JSON.parseObject(message, Message.class);
        }catch (Exception e){
            log.error("{}-{} send message format error: {}",cid, session.getId(), e);
        }
    }
    @OnError
    public void onError(Session session, Throwable error, @PathParam("cid") String cid) {
        log.error("{}-{} ERROR: {}", cid, session.getId(), error);
    }

    public static void broadcastMessage(Message message){

    }

    public static void broadcastMessage(String message){
        for(Map.Entry<String, Session> entry : webSocketServersMap.entrySet()){
            if(entry.getValue().isOpen()){
                sendMessage(entry.getKey(), entry.getValue(), message);
            }
        }
    }

    public static void sendMessage(String cid, Session session, String message){
        try {
            session.getBasicRemote().sendText(message);
        } catch (IOException e) {
            log.error("Send message to {}-{} ERROR: {}", cid, session.getId(), e);
        }
    }

    @Data
    static class Message{
        /**
         * 消息时间
         */
        private Long createTime;
        /**
         * 消息类型
         */
        private Integer mtype;

        private String from;
        private String to;
        /**
         * 消息
         */
        private String message;

        /**
         * 消息内容
         */
        private Object content;

        public Message(Long createTime, Integer mtype, String from, String to, String message, Object content) {
            this.createTime = createTime;
            this.mtype = mtype;
            this.from = from;
            this.to = to;
            this.message = message;
            this.content = content;
        }
        public Message(String from, String to, String message) {
            this.from = from;
            this.to = to;
            this.message = message;
            this.createTime = System.currentTimeMillis();
            this.mtype = 0;
        }

        public Message(String from, String to, String message, Object content) {
            this.from = from;
            this.to = to;
            this.message = message;
            this.content = content;
            this.createTime = System.currentTimeMillis();
            this.mtype = 0;
        }

        public Message() {
        }
    }
}
