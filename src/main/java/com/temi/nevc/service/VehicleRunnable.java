package com.temi.nevc.service;

import com.alibaba.fastjson.JSON;
import com.temi.nevc.common.util.DateUtil;
import com.temi.nevc.entity.Vehicle;
import com.temi.nevc.service.ViewVO;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.*;

/**
 * @author ccl
 * @time 2018-09-21 14:48
 * @name VehicleRunnable
 * @desc:
 */
@Slf4j
@Component
public class VehicleRunnable implements Runnable{

    @Resource
    StringRedisTemplate stringRedisTemplate;

    private final List<Vehicle> vehicles = new LinkedList<>();

    private String[] models = new String[]{"唐DM","奥迪A6 e-tron", "领克01 PHEV", "蔚来ES8", "Model X", "Model S",
    "元EV", "荣威MARVEL X", "荣威ERX5", "帝豪EV", "宋EV", "比亚迪e6", "秦EV", "众泰E200", "长安奔奔EV", "比亚迪e5"};
    private String[] licenses = new String[]{"京A.D0800", "京N.C1279", "京F.F1123", "京M.N1871", "京B.A1315", "京C.A1200","京Q.W3533", "京Q.E2233",
            "京A.B0871", "京N.Q1091", "京F.C1323", "京M.V1872", "京B.C1615", "京K.A1240","京Q.L3573", "京L.E2133"};
    private int preIndex = 0;
    private void produceVehicle(){
        Random indexRandom = new Random();
        int index = indexRandom.nextInt(16);
        if(index != preIndex){
            Random batteryRandom1 = new Random();
            Random batteryRandom2 = new Random();
            double battery = batteryRandom1.nextInt(300) + batteryRandom2.nextDouble() + 20D;

            Random odoRandom1 = new Random();
            Random odoRandom2 = new Random();
            double odo = odoRandom1.nextInt(3000) + odoRandom2.nextDouble() + 100D;

            Random ageRandom = new Random();
            double age = ageRandom.nextInt(8) + 1;

            Vehicle vehicle = new Vehicle(licenses[index], models[index], battery, odo, age);

            if(vehicles.size() < 10){
                vehicles.add(0, vehicle);
            }else {
                vehicles.remove(vehicles.size() - 1);
                vehicles.add(0, vehicle);
            }

            Long time = System.currentTimeMillis();

            ViewVO vo = new ViewVO();
            vo.setCreateTime(time);
            vo.setRegion(1);
            vo.setVehicles(vehicles);
            stringRedisTemplate.convertAndSend("block", JSON.toJSONString(vo));
        }
    }

    public List<Vehicle> getVehicles(int pageSize, int pageNum) {
        if(pageSize > 1 && vehicles.size() >= pageSize){
            return vehicles.subList(0, pageSize);
        }
        return vehicles;
    }

    @Override
    public void run() {
        produceVehicle();
    }
}
