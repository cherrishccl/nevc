package com.temi.nevc.service;

import com.alibaba.fastjson.JSON;
import com.temi.nevc.common.redis.RedisService;
import com.temi.nevc.common.util.ArithmeticUtils;
import com.temi.nevc.common.util.DateUtil;
import com.temi.nevc.common.util.RedisConst;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author ccl
 * @time 2018-09-20 18:07
 * @name BlockRunnable
 * @desc:
 */
@Slf4j
@Component
public class BlockRunnable implements Runnable{
    private final AtomicLong block = new AtomicLong(0L);
    private Long latestBlock = 0L;

    public Long getLatestBlock() {
        return latestBlock;
    }

    @Resource
    private RedisService redisService;

    @Resource
    StringRedisTemplate stringRedisTemplate;

    private void produceBlock(){
        Long b = block.incrementAndGet();
        Long l = null;
        try{
            l = (Long) redisService.get(RedisConst.BLOCK_LATEST);
        }catch (Exception e){
            log.error("total score not save in redis", e);
        }
        if(null != l && (b-l) < 0){
            block.set(l);
        }
        latestBlock = b;

        redisService.setNx(RedisConst.BLOCK_LATEST, latestBlock);

        Long time = System.currentTimeMillis();
        String curTime = DateUtil.long2TimeString(time);

        ViewVO vo = new ViewVO();
        vo.setCreateTime(time);
        vo.setRegion(1);
        vo.setLatestBlock(latestBlock);
        stringRedisTemplate.convertAndSend("block", JSON.toJSONString(vo));

        log.info("{}, Latest block : {}", curTime, latestBlock);

    }
    @Override
    public void run() {
        produceBlock();
    }
}
