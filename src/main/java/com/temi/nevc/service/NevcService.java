package com.temi.nevc.service;

import com.alibaba.fastjson.JSON;
import com.temi.nevc.common.model.RestResp;
import com.temi.nevc.common.util.DateUtil;
import com.temi.nevc.contract.ContractCo2;
import com.temi.nevc.entity.Vehicle;
import com.temi.nevc.entity.VehicleScore;
import com.temi.nevc.repo.VehicleRepo;
import com.temi.nevc.repo.VehicleScoreRepo;
import com.temi.nevc.schedule.BlockScheduler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import static com.temi.nevc.schedule.BlockScheduler.LTEST_BLOCK_10;
import static com.temi.nevc.schedule.BlockScheduler.LTEST_BLOCK_HASH_10;

/**
 * @author ccl
 * @time 2018-09-20 16:36
 * @name NevcService
 * @desc:
 */
@Slf4j
@Service
public class NevcService {

    @Resource
    ScoreRunnable scoreRunnable;
    @Resource
    BlockRunnable blockRunnable;
    @Resource
    VehicleRunnable vehicleRunnable;

    @Resource
    private VehicleRepo vehicleRepo;
    @Resource
    private VehicleScoreRepo vehicleScoreRepo;

    private static Filter filter = new Filter();

    public RestResp totalScore(){
        Long time = System.currentTimeMillis();

        ViewVO vo = new ViewVO();
        vo.setCreateTime(time);
        vo.setRegion(1);
//        vo.setTotalScore(scoreRunnable.getTotalScore());
        totalScore(0);
        vo.setTotalScore(tscore);
        return RestResp.success(vo);
    }

    public RestResp increaseScore(){
        Long time = System.currentTimeMillis();

        ViewVO vo = new ViewVO();
        vo.setCreateTime(time);
        vo.setRegion(1);
//        vo.setIncrease(scoreRunnable.getIncreaseScore());

        double inc = totalScore(0);
        inc = inc * 500;
        if(inc <= 0){
            inc = Math.random() / 150 + 0.00000199 * 5000 * 0.6;
        }
        if(inc > 1){
            inc = Math.random() / 150 + 0.00000199 * 5000 * 0.6;
        }
        vo.setIncrease(inc);
        return RestResp.success(vo);
    }

    public RestResp vehicleData(Integer pageSize, Integer pageNum){
        Long time = System.currentTimeMillis();
        ViewVO vo = new ViewVO();
        vo.setCreateTime(time);
        vo.setRegion(1);
        vo.setTotalCar(scoreRunnable.getTotalCar());
        vo.setVehicles(vehicleRunnable.getVehicles(pageSize, pageNum));

        Long createTime = DateUtil.getCurrentTimeMills(10L);
//        pageNum = ((time/1000) % 10) == 0 ? 10 : (int)((time/1000) % 10);
        Random random = new Random();
        pageNum = random.nextInt(10);
        Pageable pageable = PageRequest.of(pageNum, pageSize);
       Page<Vehicle>  vehicles = vehicleRepo.findByCreateTime(createTime, pageable);
        vo.setVehicles(vehicles.getContent());
        return RestResp.success(vo);
    }

    public RestResp vehicleData(String vehicleNo){
        return RestResp.success();
    }

    public RestResp latestBlock(){
        Long time = System.currentTimeMillis();

        ViewVO vo = new ViewVO();
        vo.setCreateTime(time);
        vo.setRegion(1);
        //vo.setLatestBlock(blockRunnable.getLatestBlock());
        vo.setLatestBlock(LTEST_BLOCK_10.size() == 0 ? 0L : LTEST_BLOCK_10.get(0).longValue());
        vo.setLatestBlockHash(LTEST_BLOCK_HASH_10.size() == 0 ? "" : LTEST_BLOCK_HASH_10.get(0));
        return RestResp.success(vo);
    }

    private static double tscore = 0D;

    private double totalScore(long t){
        long time =DateUtil.getCurrentTimeMills(10) - 40000L;
        List<VehicleScore> list = vehicleScoreRepo.findByCreateTime(time);
        List<VehicleScore> reslist = null;
        double iscore = 0D;
        if(null != list && list.size() > 0){
            iscore = 0d;
            reslist = new ArrayList<>(list.size());
            for(VehicleScore vehicleScore : list){
                /*try {
                    Double preco2 = ContractCo2.getInstance("320c67a11b523cc3ff174d36692000a2d5da3b15d185ec188f325744bcba4c0a").getElectricCarCO2(vehicleScore.getCarId(), time - 10000L);
                    Double precomco2 = ContractCo2.getInstance("320c67a11b523cc3ff174d36692000a2d5da3b15d185ec188f325744bcba4c0a").getCombustionCarCO2(vehicleScore.getCarId(), time - 10000L);

                    Double co2 = ContractCo2.getInstance("320c67a11b523cc3ff174d36692000a2d5da3b15d185ec188f325744bcba4c0a").getElectricCarCO2(vehicleScore.getCarId(), time);
                    Double comco2 = ContractCo2.getInstance("320c67a11b523cc3ff174d36692000a2d5da3b15d185ec188f325744bcba4c0a").getCombustionCarCO2(vehicleScore.getCarId(), time);

                    log.info("******************************* preCO2: {} --- CO2: {} --- COMCO2: {} --- preCOMCO2: {}", preco2, co2, comco2, precomco2);

                    double diffco2 = preco2 - co2;
                    double diffcomco2 = comco2 - precomco2;
                    iscore += (diffcomco2 - diffco2);
                    tscore = tscore + iscore < 0 ? 0 : iscore;

                    vehicleScore.setCo2(diffco2);
                    vehicleScore.setComco2(comco2);
                    reslist.add(vehicleScore);
                } catch (IOException e) {
                    e.printStackTrace();
                }*/
                iscore = iscore +  (vehicleScore.getScore() == null ? 0 : vehicleScore.getScore());
            }
            tscore = tscore + iscore;
        }
        if(null != reslist && reslist.size() > 0){
            //vehicleScoreRepo.saveAll(reslist);
        }
        return iscore;
    }
}
