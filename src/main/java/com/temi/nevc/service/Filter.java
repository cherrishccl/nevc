package com.temi.nevc.service;

import java.util.concurrent.ArrayBlockingQueue;

public class Filter {
    private ArrayBlockingQueue<Double> data = new ArrayBlockingQueue<Double>(31);

    public Filter() {
        for (int i = 0; i != 30; i++) {
            data.add(0.0);
        }
    }

    public void insert(double da) {
        if(data.size() > 30){
            data.poll();
        }
        Double x = 0.0;
        if (da < 0){
            x = 1.995*250;
        }else {
            x = da;
        }
        x = x * 0.055;
        data.add(x);
        data.poll();
    }

    public double get() {
        Double avg = 0.0;
        for (Double i : data) {
            avg += i;
        }
        avg = avg / 30;
        return avg.doubleValue();
    }
}
