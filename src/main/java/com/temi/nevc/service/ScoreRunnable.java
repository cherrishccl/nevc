package com.temi.nevc.service;

import com.alibaba.fastjson.JSON;
import com.temi.nevc.common.redis.RedisService;
import com.temi.nevc.common.util.ArithmeticUtils;
import com.temi.nevc.common.util.DateUtil;
import com.temi.nevc.common.util.RedisConst;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Component;

import javax.annotation.Resource;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author ccl
 * @time 2018-09-20 16:53
 * @name ScoreRunnable
 * @desc:
 */
@Component
@Slf4j
public class ScoreRunnable implements Runnable{

    @Resource
    private StringRedisTemplate stringRedisTemplate;

    @Resource
    private RedisService redisService;

    private Integer totalCar = 23;
    private Double totalScore = 0D;
    private Double increaseScore =0D;

    public Integer getTotalCar() {
        return totalCar;
    }

    public Double getTotalScore() {
        return totalScore;
    }

    public Double getIncreaseScore() {
        return increaseScore;
    }


    private void produceScore(){
        increaseScore = Math.random();
        if(totalScore == 0){
            try{
                Double t = (Double) redisService.get(RedisConst.SCORE_TOTAL);
                if(null != t){
                    totalScore = t;
                }
            }catch (Exception e){
                log.error("total score not save in redis", e);
            }
        }
        totalScore = ArithmeticUtils.plus(totalScore, increaseScore);

        redisService.setNx(RedisConst.SCORE_TOTAL, totalScore);
        redisService.setNx(RedisConst.SCORE_INCREASE, increaseScore);
        redisService.setNx(RedisConst.CAR_TOTAL, totalCar);

        Long time = System.currentTimeMillis();
        String curTime = DateUtil.long2TimeString(time);

        ViewVO vo = new ViewVO();
        vo.setCreateTime(time);
        vo.setRegion(1);
        vo.setIncrease(increaseScore);
        vo.setTotalCar(totalCar);
        vo.setTotalScore(totalScore);

        stringRedisTemplate.convertAndSend("block", JSON.toJSONString(vo));

        log.info("{}, Increase Score: {}", curTime, increaseScore);
        log.info("{}, Total Score: {}", curTime, totalScore);
    }

    @Override
    public void run() {
        produceScore();
    }
}
