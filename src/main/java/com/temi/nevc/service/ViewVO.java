package com.temi.nevc.service;

import com.temi.nevc.entity.Vehicle;
import lombok.Data;

import java.util.List;

/**
 * @author ccl
 * @time 2018-09-20 16:48
 * @name ViewVO
 * @desc:
 */
@Data
public class ViewVO {
    private Long createTime;
    private Integer region;
    private Integer totalCar;
    private Double totalScore;
    private Double increase;
    private Long latestBlock;
    private String latestBlockHash;
    List<Vehicle> vehicles;
}
