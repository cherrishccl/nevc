package com.temi.nevc.service.produce;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

/**
 * @author cherrish
 * @time 2018-08-30 15:01
 * @name MessageSender
 * @desc:
 */
@EnableScheduling
@Component
public class MessageSender {
    @Autowired
    private StringRedisTemplate stringRedisTemplate;

    //@Scheduled(fixedRate = 2000)
    public void sendMessage1(){
        stringRedisTemplate.convertAndSend("score", "score ---> " + String.valueOf(Math.random()));
    }

    //@Scheduled(fixedRate = 4000)
    public void sendMessage2(){
        stringRedisTemplate.convertAndSend("block", "block ---> " + String.valueOf(Math.random()));
    }

    //@Scheduled(fixedRate = 6000)
    public void sendMessage3(){
        stringRedisTemplate.convertAndSend("vehicle", "vehicle ---> " + String.valueOf(Math.random()));
    }
}
