package com.temi.nevc.service.produce;

import com.temi.nevc.websocket.WebSocketServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

/**
 * @author cherrish
 * @time 2018-08-30 15:05
 * @name MessageReceiver
 * @desc:
 */
@Slf4j
@Component
public class MessageReceiver {
    public void receiveMessage(String message){
        log.info("receive a message: {}" , message);
        WebSocketServer.broadcastMessage(message);
    }
}
