package com.temi.nevc.service;


import com.temi.nevc.common.model.JobData;
import com.temi.nevc.common.model.RestResp;
import com.temi.nevc.common.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Bean;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

import org.springframework.scheduling.support.CronTrigger;
import org.springframework.stereotype.Service;

import javax.annotation.Resource;

import java.util.concurrent.ScheduledFuture;

/**
 * @author ccl
 * @time 2018-09-13 15:43
 * @name ScheduleService
 * @desc:
 */
@Slf4j
@Service
public class ScheduleService {
    @Resource
    private ThreadPoolTaskScheduler threadPoolTaskScheduler;


    @Resource
    private ScoreRunnable scoreRunnable;
    @Resource
    private BlockRunnable blockRunnable;
    @Resource
    private VehicleRunnable vehicleRunnable;

    /**
     * 在ScheduledFuture中有一个cancel可以停止定时任务。
     */
    private final String[] T_NAME = new String[]{"score", "block", "vehicle"};
    private ScheduledFuture<?> score;
    private ScheduledFuture<?> block;
    private ScheduledFuture<?> vehicle;

    /**
     * ThreadPoolTaskScheduler：线程池任务调度类，能够开启线程池进行任务调度。
     * ThreadPoolTaskScheduler.schedule()方法会创建一个定时计划ScheduledFuture，在这个方法需要添加两个参数，Runnable（线程接口类） 和CronTrigger（定时任务触发器）
     */
    @Bean
    public ThreadPoolTaskScheduler threadPoolTaskScheduler() {
        return new ThreadPoolTaskScheduler();
    }

    public RestResp startJob(JobData job){
        if(null == job || null == job.getCron() || "".equals(job.getCron().trim()) || null == job.getTname() || "".equals(job.getTname().trim())){
            return RestResp.fail("Task start failure, please give cron , tname or other required parameters.");
        }
        String time = DateUtil.getPresentDate();
        if(T_NAME[0].equals(job.getTname())){
            score = threadPoolTaskScheduler.schedule(scoreRunnable, new CronTrigger(job.getCron()));
            log.info("Start SCORE job: cron = {}, start time: {}", job.getCron(), time);
        }else if(T_NAME[1].equals(job.getTname())){
            block = threadPoolTaskScheduler.schedule(blockRunnable, new CronTrigger(job.getCron()));
            log.info("Start BLOCK job: cron = {}, start time: {}", job.getCron(), time);
        }else if(T_NAME[2].equals(job.getTname())){
            log.info("Start VEHICLE job, start time: {}", time);
            vehicle = threadPoolTaskScheduler.schedule(vehicleRunnable, new CronTrigger(job.getCron()));
        }else {
            return RestResp.fail("Job not exist");
        }
        return RestResp.success("Task Start Success", job);
    }
    public RestResp stopJob(JobData job){
        if(null == job || null == job.getTname() || "".equals(job.getTname().trim())){
            return RestResp.fail("Task start failure, please give tname or other required parameters.");
        }
        String time = DateUtil.getPresentDate();
        if(T_NAME[0].equals(job.getTname())){
            if(null != score){
                score.cancel(true);
            }
            log.info("Stop SCORE job: cron = {}, stop time: {}", job.getCron(), time);
        }else if(T_NAME[1].equals(job.getTname())){
            if(null != block){
                block.cancel(true);
            }
            log.info("Stop BLOCK job: cron = {}, stop time: {}", job.getCron(), time);
        }else if(T_NAME[2].equals(job.getTname())){
            if(null != vehicle){
                vehicle.cancel(true);
            }
            log.info("Stop VEHICLE job:cron = {}, stop time: {}", job.getCron(), time);
        }else {
            return RestResp.fail("Job not exist");
        }
        return RestResp.success("Task Stop Success");
    }

    public RestResp changeJob(JobData job){
        if(null == job || null == job.getCron() || "".equals(job.getCron().trim()) || null == job.getTname() || "".equals(job.getTname().trim())){
            return RestResp.fail("Task start failure, please give cron , tname or other required parameters.");
        }
        RestResp restResp = stopJob(job);
        if(-1 == restResp.status){
            return RestResp.fail("Task change error occured when stop it.");
        }
        restResp = stopJob(job);
        if(-1 == restResp.status){
            return RestResp.fail("Task change error occured when restart it.");
        }
        return RestResp.success("Task Change Success");
    }
}
