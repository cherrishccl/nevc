package com.temi.nevc.rest;

import com.temi.nevc.common.model.RestResp;
import com.temi.nevc.common.reqlimit.RequestLimit;
import com.temi.nevc.service.NevcService;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;

/**
 * @author ccl
 * @time 2018-09-20 16:21
 * @name NevcController
 * @desc:
 */
@RequestMapping(value = "/nevc")
@RestController
public class NevcController {

    @Resource
    private NevcService nevcService;

    @RequestLimit(count = 5, time = 15000)
    @GetMapping(value = "/score/total")
    public RestResp totalScore(){
        return nevcService.totalScore();
    }

    @RequestLimit(count = 5, time = 15000)
    @GetMapping(value = "/score/increase")
    public RestResp increaseScore(){
        return nevcService.increaseScore();
    }

    @RequestLimit(count = 5, time = 15000)
    @GetMapping(value = "/vehicle/{pageSize}/{pageNum}")
    public RestResp vehicleData(@PathVariable Integer pageSize, @PathVariable Integer pageNum){
        return nevcService.vehicleData(pageSize, pageNum);
    }

    @RequestLimit(count = 5, time = 25000)
    @GetMapping(value = "/vehicle/{vehicleNo}")
    public RestResp vehicleData(@PathVariable String vehicleNo){
        return nevcService.vehicleData(vehicleNo);
    }

    @RequestLimit(count = 5, time = 15000)
    @GetMapping(value = "/block/latest")
    public RestResp latestBlock(){
        return nevcService.latestBlock();
    }
}
