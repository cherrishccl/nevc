package com.temi.nevc.rest;

import com.temi.nevc.common.model.RestResp;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author ccl
 * @time 2018-09-20 14:36
 * @name InfoController
 * @desc:
 */
@RestController
public class InfoController {
    @GetMapping(value = "/welcome")
    public RestResp welcome(){
        return RestResp.success("Welcome to NEVC");
    }
}
