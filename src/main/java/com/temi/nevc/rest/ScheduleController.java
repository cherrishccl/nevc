package com.temi.nevc.rest;

import com.temi.nevc.common.model.JobData;
import com.temi.nevc.common.model.RestResp;
import com.temi.nevc.service.ScheduleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.annotation.Resource;


/**
 * @author ccl
 * @time 2018-09-13 15:37
 * @name ScheduleController
 * @desc:
 */
@Slf4j
@RestController
@RequestMapping(value = "/schedule")
public class ScheduleController {
    @Resource
    private ScheduleService scheduleService;

    @PostMapping(value = "/start")
    public RestResp startJob(@RequestBody JobData job){
        return scheduleService.startJob(job);
    }
    @PostMapping(value = "/stop")
    public RestResp stopJob(@RequestBody JobData job){
        return scheduleService.stopJob(job);
    }
    @PostMapping(value = "/change")
    public RestResp changeJob(@RequestBody JobData job){
        return scheduleService.changeJob(job);
    }
}
