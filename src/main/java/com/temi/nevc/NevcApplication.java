package com.temi.nevc;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@EnableScheduling
@SpringBootApplication
public class NevcApplication {

    public static void main(String[] args) {
        SpringApplication.run(NevcApplication.class, args);
    }
}
