package com.temi.nevc.entity;

import cn.afterturn.easypoi.excel.annotation.Excel;
import lombok.Data;

import javax.persistence.*;

/**
 * @author ccl
 * @time 2018-08-20 14:22
 * @name Vehicle
 * @desc:
 */
@Entity
@Table(name = "vehicle4")
@Data
public class TVehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    @Excel(name = "车ID")
    private Integer carId;
    @Excel(name = "车牌号")
    private String license;
    @Excel(name = "时间")
    private String rawTime;
    private String model;
    private Long createTime;
    private String curTime;
    @Excel(name = "电压")
    private Double voltage;
    @Excel(name = "电量")
    private Double energy;
    @Excel(name = "速度")
    private Double speed;
    @Excel(name = "里程数A")
    private Double odoa;
    @Excel(name = "电池能量")
    private Double battery;
    @Excel(name = "里程数")
    private Double odo;



}
