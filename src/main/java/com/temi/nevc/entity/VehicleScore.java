package com.temi.nevc.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * @author ccl
 * @time 2018-09-28 16:45
 * @name VehicleScore
 * @desc:
 */
@Table(name = "vehicle_score")
@Entity
@Data
public class VehicleScore {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private String txhash;
    private Integer carId;
    private Long createTime;
    private Double co2;
    private Double comco2;
    private Double preco2;
    private Double precomco2;
    private Double score;

    public VehicleScore() {
    }

    public VehicleScore(String txhash, Integer carId, Long createTime) {
        this.txhash = txhash;
        this.carId = carId;
        this.createTime = createTime;
    }
}
