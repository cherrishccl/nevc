package com.temi.nevc.entity;

import lombok.Data;

import javax.persistence.*;

/**
 * @author ccl
 * @time 2018-09-27 9:36
 * @name Vehicle
 * @desc:
 */
@Data
@Entity
@Table(name = "t_vehicle")
public class Vehicle {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private Integer carId;
    private Long createTime;
    private String curTime;
    private String rawTime;
    private Double voltage;
    private Double speed;
    private String license;
    private String model;
    private Double battery;
    private Double odoa;
    private Double age;
    private Double odo;

    public Vehicle() {
    }

    public Vehicle(String license, String model, Double battery, Double odo,Double age) {
        this.license = license;
        this.model = model;
        this.battery = battery;
        this.odo = odo;
        this.age = age;
    }
}
