package com.temi.nevc.common.model;

import lombok.Data;

/**
 * @author ccl
 * @time 2018-09-13 15:47
 * @name JobParam
 * @desc:
 */
@Data
public class JobData {
    private Long tid;
    private String tname;
    private Long time;
    private String cron;

    private Long start;
    private Long end;
    private Long step;

}
