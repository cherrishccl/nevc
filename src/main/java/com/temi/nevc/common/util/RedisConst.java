package com.temi.nevc.common.util;

/**
 * @author ccl
 * @time 2018-09-20 17:10
 * @name RedisConsts
 * @desc:
 */
public class RedisConst {
    public final static String CAR_TOTAL = "car_total";
    public final static String SCORE_TOTAL = "score_total";
    public final static String SCORE_INCREASE = "score_increase";

    public final static String BLOCK_LATEST = "block_latest";

    public final static String VEHICLE_01 = "vehicle_01";
    public final static String VEHICLE_02 = "vehicle_02";
    public final static String VEHICLE_03 = "vehicle_03";
    public final static String VEHICLE_04 = "vehicle_04";
    public final static String VEHICLE_05 = "vehicle_05";
    public final static String VEHICLE_06 = "vehicle_06";
    public final static String VEHICLE_07 = "vehicle_07";
    public final static String VEHICLE_08 = "vehicle_08";
    public final static String VEHICLE_09 = "vehicle_09";
}
