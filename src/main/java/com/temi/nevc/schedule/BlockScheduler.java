package com.temi.nevc.schedule;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONObject;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.EthBlock;
import org.web3j.protocol.http.HttpService;
import rx.Subscription;

import java.math.BigInteger;
import java.util.LinkedList;
import java.util.concurrent.ConcurrentSkipListMap;

/**
 * @author ccl
 * @time 2018-06-25 11:45
 * @name EthTimer
 * @desc:
 */
@Component
@Slf4j
public class BlockScheduler {
    private  static Web3j web3j = null;
    private Subscription subscription = null;

    @Value("${themis.blockinfo.server}")
    private String themisBlockServer;

    public final static LinkedList<BigInteger> LTEST_BLOCK_10 = new LinkedList<>();
    public final static LinkedList<String> LTEST_BLOCK_HASH_10 = new LinkedList<>();
    @Scheduled(cron = "0/30 * * * * ?")
    public void subscribe(){
        try {
            if(null == web3j || subscription == null){
                log.info("SERVER_URL: {}",themisBlockServer);
                web3j = Web3j.build(new HttpService(themisBlockServer));
                subscription = web3j.blockObservable(true).subscribe(ethBlock -> {
                    EthBlock.Block block = ethBlock.getBlock();
                    BigInteger latestNum = block.getNumber();
                    String latestBlockHash = block.getHash();
                    LTEST_BLOCK_10.add(0, latestNum);
                    LTEST_BLOCK_HASH_10.add(0, latestBlockHash);
                    if(LTEST_BLOCK_10.size() > 10){
                        LTEST_BLOCK_10.removeLast();
                        LTEST_BLOCK_HASH_10.removeLast();
                    }
                    log.info("Latest block: {} ---> {}", latestNum, JSONObject.toJSONString(block));
                });
            }
        }catch (Exception e){
            web3j = null;
            subscription = null;
            log.error("ETH Lister Error :", e);
        }
    }
}
