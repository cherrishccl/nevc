package com.temi.nevc.schedule;

import com.alibaba.fastjson.JSONObject;
import com.temi.nevc.common.util.DateUtil;
import com.temi.nevc.contract.Co2Contract;
import com.temi.nevc.contract.ContractCo2;
import com.temi.nevc.entity.Vehicle;
import com.temi.nevc.entity.VehicleScore;
import com.temi.nevc.repo.VehicleRepo;
import com.temi.nevc.repo.VehicleScoreRepo;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import org.web3j.abi.datatypes.Utf8String;
import org.web3j.abi.datatypes.generated.Uint256;
import org.web3j.abi.datatypes.generated.Uint32;
import org.web3j.crypto.CipherException;
import org.web3j.crypto.Credentials;
import org.web3j.crypto.WalletUtils;
import org.web3j.protocol.Web3j;
import org.web3j.protocol.core.methods.response.EthBlock;
import org.web3j.protocol.http.HttpService;
import org.web3j.utils.Convert;
import rx.Subscription;

import javax.annotation.PostConstruct;
import javax.annotation.Resource;
import java.io.IOException;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.atomic.AtomicLong;

/**
 * @author ccl
 * @time 2018-06-25 11:45
 * @name EthTimer
 * @desc:
 */
@Component
@Slf4j
public class UpchainScheduler {
    private static long stime = DateUtil.getCurrentTimeMills(10);
    private static AtomicLong T_TIME = new AtomicLong(stime);
    private static AtomicLong TT_TIME = new AtomicLong(stime - 10000L);

    @Value("${themis.blockinfo.server}")
    private String themisBlockServer;
    @Value("${themis.nevc.contract.address}")
    private String nevcAddress;
    @Resource
    private VehicleScoreRepo vehicleScoreRepo;
    @Resource
    private VehicleRepo vehicleRepo;

    @Scheduled(cron = "0/10 * * * * ?")
    public void uploadInfo(){
        Pageable pageable = PageRequest.of(0, 10);
        long time = T_TIME.getAndAdd(10000L);
        log.info("TIME: {} --- T_TIME: {}", time, T_TIME.get());
        List<Vehicle> list = null;
        try{
            list = vehicleRepo.findByCreateTime(time, pageable).getContent();
        }catch (Exception e){
            log.error("TIME: {} --- DB ERROR: {}", time, e);
        }
        if(null != list && list.size() > 0){
            for(Vehicle vehicle : list){
                VehicleScore vehicleScore = null;
                try{
                    long odo = (long) (vehicle.getOdo() * 100);
                    long energy = (long) (vehicle.getBattery() * 1000);
                    String hash = ContractCo2.getInstance("320c67a11b523cc3ff174d36692000a2d5da3b15d185ec188f325744bcba4c0a").updateCarInfo(vehicle.getCarId(), time, odo, energy, vehicle.getLicense(), vehicle.getModel());
                    vehicleScore = new VehicleScore(hash, vehicle.getCarId(), time);
                }catch (Exception e){
                    log.error("UP CHAIN ERROR: {}", e);
                    vehicleScore = new VehicleScore(null, vehicle.getCarId(), time);
                }
                vehicleScoreRepo.save(vehicleScore);
            }
        }
    }
    @javax.transaction.Transactional(javax.transaction.Transactional.TxType.NEVER)
    @Scheduled(cron = "0/10 * * * * ?")
    public void getInfo(){
        long time = TT_TIME.getAndAdd(10000L);//DateUtil.getCurrentTimeMills(10) - 30000L;
        List<VehicleScore> list = null;
        try{
            list = vehicleScoreRepo.findByCreateTime(time);
        }catch (Exception e){
            log.error("DB ERROR: {}", e);
        }
        getAndSaveCO2(time, list);
    }

    @javax.transaction.Transactional(javax.transaction.Transactional.TxType.NEVER)
//    @Scheduled(cron = "0/30 * * * * ?")
    public void checkNull(){
        long time = DateUtil.getCurrentTimeMills(10) - 1800000L;
        List<VehicleScore> list = null;
        List<VehicleScore> reslist = null;
        try{
            list = vehicleScoreRepo.findByScoreIsNullAndCreateTimeGreaterThan(time);
        }catch (Exception e){
            log.error("DB ERROR: {}", e);
        }
        getAndSaveCO2(time, list);
    }
    private void getAndSaveCO2(Long time, List<VehicleScore> list){
        List<VehicleScore> reslist = null;
        if(null != list && list.size() > 0){
            reslist = new ArrayList<>(list.size());
            for(VehicleScore vehicleScore : list){
                /*VehicleScore vehicleScore1 = getCO2(time, vehicleScore);
                reslist.add(vehicleScore1);*/
                getCO2(time, vehicleScore);
                vehicleScoreRepo.save(vehicleScore);
            }
            /*if(reslist.size() > 0){
                vehicleScoreRepo.saveAll(reslist);
            }*/
        }
    }
    private VehicleScore getCO2(Long time, VehicleScore vehicleScore){
        if(null != vehicleScore){
            try{
                Double preco2 = ContractCo2.getInstance("320c67a11b523cc3ff174d36692000a2d5da3b15d185ec188f325744bcba4c0a").getElectricCarCO2(vehicleScore.getCarId(), time - 10000L);
                Double precomco2 = ContractCo2.getInstance("320c67a11b523cc3ff174d36692000a2d5da3b15d185ec188f325744bcba4c0a").getCombustionCarCO2(vehicleScore.getCarId(), time - 10000L);

                Double co2 = ContractCo2.getInstance("320c67a11b523cc3ff174d36692000a2d5da3b15d185ec188f325744bcba4c0a").getElectricCarCO2(vehicleScore.getCarId(), time);
                Double comco2 = ContractCo2.getInstance("320c67a11b523cc3ff174d36692000a2d5da3b15d185ec188f325744bcba4c0a").getCombustionCarCO2(vehicleScore.getCarId(), time);
                double co21 = preco2 -co2;
                double comco21 = comco2 -precomco2;
                vehicleScore.setPreco2(preco2);
                vehicleScore.setCo2(co2);
                vehicleScore.setComco2(comco2);
                vehicleScore.setPrecomco2(precomco2);
                vehicleScore.setScore(comco21 - co21);
            }catch (Exception e){
                log.error("Get CO2 ERROR: {}", e);
            }
        }
        return vehicleScore;
    }
}
